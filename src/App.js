import React from 'react';
import './index.css';
import {
    Route,
    BrowserRouter as Router,
    Switch,
} from "react-router-dom";
import {Game} from "./components/game/Game";
import {Leaderboard} from "./components/Leaderboard";
import Login from './components/player/LogIn';
import {CreateRoom} from "./components/Room/CreateRoom";

export default function App() {
    return(
    <Router>
        <Switch>
            <Route exact path="/">
                <CreateRoom />
            </Route>
            <Route exact path="/room/:id">
                <Game />
            </Route>
            <Route exact path="/leaderboard">
                <Leaderboard />
            </Route>
            <Route exact path="/login">
                <Login />
            </Route>
        </Switch>
    </Router>
    );
}
