import React, {useEffect, useState} from 'react';
import '../../index.css';
import Navbar from "../navbar/Navbar";
import CalculateWinner from "./Helper";
import Board from "./Board";
import firebase from "firebase";

export const Game = () => {

    const [board, setBoard] = useState(Array(9).fill(null));
    const [xIsNext, setXIsNext] = useState(true);
    const winner = CalculateWinner(board);

    const db = firebase.firestore().collection('rooms');
    const [code, setCode] = useState();
    const [player1, setPlayer1] = useState('');
    const [player2, setPlayer2] = useState('');

    db.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            setCode(doc.id);
        });
    });

    useEffect(() => {
        db.doc(code).get().then((doc) => {
            if (!doc.exists) return
            setPlayer1(doc.data().player1)
            setPlayer2(doc.data().player2)
        });
    }, [])

    const handleClick = (index) => {
        const boardCopy = [...board]
        if (winner || boardCopy[index]) return
        boardCopy[index] = xIsNext ? 'X' : 'O'
        setBoard(boardCopy)
        setXIsNext(!xIsNext)
    }

    const startNewGame = () => {
        return (
            <button className="clear-btn" onClick={() => setBoard(Array(9).fill(null))}>Repeat</button>
        )
    }

    const leaveRoom = () => {
        return (
            <button className="leave-btn">Leave Room</button>
        )
    }

    return (
        <div>
            <Navbar />
            <div className="wrapper">
                <Board squares={board} click={handleClick} />
                <div className="game-info">
                    {winner ? "Winner: " + winner : "Next player: " + (xIsNext ? player1 + "(X)" : player2 + "(O)")}
                </div>
                {startNewGame()}
                <br/>
                {leaveRoom()}
            </div>
        </div>
    )
}
