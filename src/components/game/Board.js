import React, {useEffect, useState} from "react";
import Square from "./Square";
import '../../index.css';
import firebase from "firebase";

const Board = ({squares, click}) => {

    const [code, setCode] = useState();
    const db = firebase.firestore().collection('rooms');

    const [square0, setSquare0] = useState();
    const [square1, setSquare1] = useState();
    const [square2, setSquare2] = useState();
    const [square3, setSquare3] = useState();
    const [square4, setSquare4] = useState();
    const [square5, setSquare5] = useState();
    const [square6, setSquare6] = useState();
    const [square7, setSquare7] = useState();
    const [square8, setSquare8] = useState();

    db.onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            setCode(doc.id);
        });
    });
    db.doc(code).get().then((doc) => {
        if (!doc.exists) return
        setSquare0(doc.data().square0);
        setSquare1(doc.data().square1);
        setSquare2(doc.data().square2);
        setSquare3(doc.data().square3);
        setSquare4(doc.data().square4);
        setSquare5(doc.data().square5);
        setSquare6(doc.data().square6);
        setSquare7(doc.data().square7);
        setSquare8(doc.data().square8);
    });

    return (
        <div className="board">
            {
                squares.map((square, i) => {
                    switch (i) {
                        case 0:
                            db.doc(code).update({
                                square0: square
                            })
                            console.log(square0)
                            break
                        case 1:
                            db.doc(code).update({
                                square1: square
                            })
                            console.log(square1)
                            break
                        case 2:
                            db.doc(code).update({
                                square2: square
                            })
                            console.log(square2)
                            break
                        case 3:
                            db.doc(code).update({
                                square3: square
                            })
                            console.log(square3)
                            break
                        case 4:
                            db.doc(code).update({
                                square4: square
                            })
                            console.log(square4)
                            break
                        case 5:
                            db.doc(code).update({
                                square5: square
                            })
                            console.log(square5)
                            break
                        case 6:
                            db.doc(code).update({
                                square6: square
                            })
                            console.log(square6)
                            break
                        case 7:
                            db.doc(code).update({
                                square7: square
                            })
                            console.log(square7)
                            break
                        case 8:
                            db.doc(code).update({
                                square8: square
                            })
                            console.log(square8)
                            break
                        default:
                            break
                    }
                    return (<Square key={i} value={square} onClick={() => click(i)} />)
                })
            }
        </div>
    )
}

export default Board