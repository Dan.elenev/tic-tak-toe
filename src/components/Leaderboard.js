import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from "../components/navbar/Navbar";
import {GetPhone, GetScore} from "./player/GetProfile"

function CountId () {
    const [countId, setCountId] = useState(1);

    return (
        <th scope="col">{countId}</th>
    );
}

export function Leaderboard() {

    return (
        <div>
            <Navbar />
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Player</th>
                    <th scope="col">Score</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <CountId />
                    <GetPhone />
                    <GetScore />
                </tr>
                </tbody>
            </table>
        </div>
    );
}
