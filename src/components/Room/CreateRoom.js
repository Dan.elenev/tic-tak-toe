import React, {useState} from "react";
import firebase from "firebase";
import {auth} from "../../firebase/config";
import "./rooms.css"
import Navbar from "../navbar/Navbar";
import {ConnectRoom} from "./ConnectRoom";
import {
    Link
} from "react-router-dom";

export const CreateRoom = () => {

    const [code, setCode] = useState('');

    const db = firebase.firestore().collection('rooms');

    const DropDawnCreate = () => {
        document.getElementById("create-room-window").classList.toggle("show");

        db.add({
            player1: auth.currentUser.phoneNumber,
            square0: "",
            square1: "",
            square2: "",
            square3: "",
            square4: "",
            square5: "",
            square6: "",
            square7: "",
            square8: ""
        });
        db.onSnapshot((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                setCode(doc.id);
            });
        });
    }

    window.onclick = function(event) {
        if (!event.target.matches('.createButton')) {

            let dropdowns = document.getElementsByClassName("dropdawn-create-window");
            let i;
            for (i = 0; i < dropdowns.length; i++) {
                let openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

    return (
        <div>
            <Navbar />
            <div className="container-button">
                <button onClick={DropDawnCreate} className="createButton">Create Room</button>
                <div id="create-room-window" className="dropdawn-create-window">
                    <p><strong>{code}</strong></p>
                    <p>You can connect to the room</p>
                    <Link to={{pathname: "/room/" + code}}><button className="btn-to-room">To the game room</button></Link>
                </div>
                <ConnectRoom />
            </div>
        </div>
    );
}