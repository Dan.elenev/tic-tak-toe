import React, {useState} from "react";
import firebase from "firebase";
import {auth} from "../../firebase/config";
import "./rooms.css"
import { useHistory } from 'react-router-dom'

export const ConnectRoom = () => {

    const [code, setCode] = useState("")

    const history = useHistory();

    const Connect = (event) => {
        event.preventDefault();
        const db = firebase.firestore().collection('rooms').doc(code);
        setTimeout(() => {
            db.update({
                player2: auth.currentUser.phoneNumber
            });
        }, 1000);
        history.push("/room/" + code)
    }

    const dropDawnLogin = () => {
        document.getElementById("login-room-window").classList.toggle("show");
    }

    return (
        <div>
            <button onClick={dropDawnLogin} className="loginButton">Connect to the Room</button>
            <div id="login-room-window" className="dropdawn-login-window">
                <p>Enter room number</p>
                <form name="publish" className='input-code-form' onSubmit={Connect}>
                    <input
                        type="text"
                        onChange={(e) => setCode(e.target.value)}
                        placeholder="Enter room number"
                        className="input-code"
                        value={code}
                    />
                    <input className="enter-code" type="submit" value="Enter"/>
                </form>
            </div>
        </div>
    );
}
