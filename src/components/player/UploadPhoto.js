import React from "react";
import {auth} from "../../firebase/config";
import firebase from "firebase"
import {GetPicture} from "./GetProfile";
import "../navbar/navbar.css"

const DisplayImage = () => {

    const imageRef = firebase.firestore().collection("users").doc(auth.currentUser.uid);

    function encodeImageFileAsURL(element) {
        let file = element.target.files[0];
        let reader = new FileReader();
        reader.onloadend = function() {
            imageRef.update({
                profilePicture: reader.result
            });
        }

        reader.readAsDataURL(file);
    }

    return (
        <div id="profile" className="dropdawn-profile">
            <div className="upload-container">
                <GetPicture />
                <input className="uploadPhoto" type="file" name="myImage" onChange={encodeImageFileAsURL} />
            </div>
            <p>{auth.currentUser.phoneNumber}</p>
        </div>
    );
}

export default DisplayImage