import React, {useState} from "react";
import {Link} from "react-router-dom";
import {auth} from "../../firebase/config";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../navbar/navbar.css"

const Logout = () => {

    const [Auth, setAuth] = useState(auth.currentUser ? true : false);

    const onLogout = () => {
            auth.signOut();
            setAuth(!Auth);
    }

    return (
        <Link onClick={onLogout} className="logout" to={{pathname: "/login"}}>
            Logout
        </Link>
    )
}

export default Logout;