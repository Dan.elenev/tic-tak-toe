import React, {useState, useEffect} from "react";
import {firebase, auth} from "../../firebase/config"

export function GetScore() {
    const [score, setScore] = useState('')

    useEffect(() => {
        const db = firebase.firestore().collection('users').doc(auth.currentUser.uid);

        db.get().then((doc) => {
            if (!doc.exists) return
            setScore(doc.data().score);
        });
    }, []);

    return (
        <td>
            {score}
        </td>
    );
}

export function GetPicture() {

    const [image, setImage] = useState('')

    const dropDawnMenu = () => {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    useEffect(() => {
        const db = firebase.firestore().collection('users').doc(auth.currentUser.uid);

        db.get().then((doc) => {
            if (!doc.exists) return
            setImage(doc.data().profilePicture);
        });
    }, []);

    return (
        <div>
            <img src={image} alt="avatar" onClick={dropDawnMenu} className="dropimg img-drop" />
        </div>
    );
}

export function GetPhone() {
    const [phone, setPhone] = useState('')

    useEffect(() => {
        const db = firebase.firestore().collection('users').doc(auth.currentUser.uid);

        db.get().then((doc) => {
            if (!doc.exists) return
            setPhone(doc.data().phone);
        });
    }, []);

    return (
        <td>
            {phone}
        </td>
    );
}