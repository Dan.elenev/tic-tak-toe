import React from "react";
import {Link} from "react-router-dom";
import "./navbar.css"
import Logout from "../player/Logout";
// import {auth} from "../../firebase/config";
import DisplayImage from "../player/UploadPhoto";
import {GetPicture} from "../player/GetProfile"

const Navbar = () => {

    const Profile = () => {
        document.getElementById("profile").classList.toggle("show");
    }

    window.onclick = function(event) {
        if (!event.target.matches('.profile')) {

            let dropdowns = document.getElementsByClassName("dropdawn-profile");
            let i;
            for (i = 0; i < dropdowns.length; i++) {
                let openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

    return (
        <nav>
            <div className="container">
                <div className="nav-link">
                    <Link aria-current="page" to={{pathname: "/"}}>Game</Link>
                    <Link to={{pathname: "/leaderboard"}}>Leaderboard</Link>
                </div>

                <div className="user">
                    <GetPicture />
                    <div id="myDropdown" className="dropdown-content">

                        <a onClick={Profile} className="profile">Profile</a>
                        <DisplayImage />
                        <Logout />
                    </div>
                </div>

            </div>
        </nav>
    );
}

export default Navbar;