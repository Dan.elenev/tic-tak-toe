import firebase from "firebase";
//import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAlJq_vvkmZQNP8KyeR-mqfmWwQ3IXTsN0",
  authDomain: "tic-tak-toe-6c80c.firebaseapp.com",
  projectId: "tic-tak-toe-6c80c",
  // storageBucket: "tic-tak-toe-e5659.appspot.com",
  // messagingSenderId: "756651715033",
  // appId: "1:280688522594:web:7ce8b9b8a0d46afc1a283b",
  // measurementId: "G-9N2DZQPTQN",
  // databaseURL: "https://tic-tak-toe-fc0d6-default-rtdb.europe-west1.firebasedatabase.app"
};



const firebase_app = firebase.initializeApp(firebaseConfig);
const auth = firebase.auth();

export {auth, firebase, firebase_app}

